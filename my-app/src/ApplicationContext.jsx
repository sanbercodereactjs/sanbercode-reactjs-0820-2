import React, { useState, createContext } from 'react'

export const ApplicationContext = createContext();

export const ApplicationProvider = (props) => {
  const [theme, setTheme] = useState('light');

  return (
    <ApplicationContext.Provider value={[theme, setTheme]}>
      {props.children}
    </ApplicationContext.Provider>
  )
}
