import React from 'react';

class Welcome extends React.Component {
	render () {
		return <p>{this.props.name}</p>;
	}
}


class ShowPrice extends React.Component {
	render () {
		return <p>{this.props.price}</p>;
	}
}

class ShowWeight extends React.Component {
	render () {
		return <p>{this.props.weight}</p>;
	}
}


let dataHargaBuah = [
  {name: "Semangka", price: 10000, weight: 1000},
  {name: "Anggur", price: 40000, weight: 500},
  {name: "Strawberry", price: 30000, weight: 400},
  {name: "Jeruk", price: 30000, weight: 1000},
  {name: "Mangga", price: 30000, weight: 500}
]

class Daftar extends React.Component {

	 constructor(props){
	    super(props)
	    this.state ={
	     name:'',
	     price:'',
	     weight:'',    
	    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.editForm = this.editForm.bind(this);
  }

  handleSubmit(event){
    this.setState({inputName: event.target.value});
  }

  editForm(event) {
  	this.setState({editForm: event.target.value});
  		
  }

  handleSubmit(event){
    event.preventDefault()
    console.log(this.state.inputName)
    this.setState({
    	name:'',
    	price:'',
    	weight:'',
    })
  }

  editForm(event){
    event.preventDefault()
    console.log(this.state.editForm)
    this.setState({
    	name:'',
    	price:'',
    	weight:'',
    })
  }


	render () {
	  	return (
			<>

				<h1>Tabel Harga Buah</h1>

				<table style={{border: "1px solid #000", padding: "2px"}}>
					<tr style={{backgroundColor: "grey"}}>
						<th style={{border: "1px solid #000"}}>Nama</th>
						<th style={{border: "1px solid #000"}}>Harga</th>
						<th style={{border: "1px solid #000"}}>Berat</th>
					</tr>


					{ dataHargaBuah.map(el => {
						return (
							<tr style={{backgroundColor: "orange"}}>
								<td style={{border: "1px solid #000"}}><Welcome name = {el.name}/></td>
					            <td style={{border: "1px solid #000"}}><ShowPrice price = {el.price}/></td>
					            <td style={{border: "1px solid #000"}}><ShowWeight weight = {(el.weight)/1000}/>kg</td>
					            <button onClick={this.editForm}>edit</button>
				  			</tr>
				        )
				  	})}


				</table>
				<form onSubmit={this.handleSubmit}>
		          <label>
		            Masukkan nama peserta:
		          </label>          
		          <input type="text" value={this.state.inputName} onChange={this.handleChange}/>
		          <button>submit</button>
		        </form>
		        <form edit={this.editForm}>
		          <label>
		            Edit nama peserta:
		          </label>          
		          <input type="text" value={this.state.editForm} onChange={this.editForm}/>
		          <button>submit</button>
		        </form>
		    </>
	    )
	}
}

export default Daftar;
