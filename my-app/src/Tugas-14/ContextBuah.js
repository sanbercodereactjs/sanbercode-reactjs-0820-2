import React, { useState, createContext } from "react";

export const ContextBuah = createContext();

export const FruitProvider = props => {

	 const [fruit, setFruit] = useState([

	 	{ id: 1, name: "Semangka", price: 10000, weight: 1000} ,
		{ id: 2, name: "Anggur", price: 40000, weight: 500 },
		{ id: 3, name: "Strawberry", price: 30000, weight: 400 },
		{ id: 4, name: "Jeruk", price: 30000, weight: 1000 },
		{ id: 5, name: "Mangga", price: 30000, weight: 500 }
	 	
	]);

	const [inputForm, setInputForm] = useState({
			name: "",
			price: 0,
			weight: 0,
			id: null
		})

	 return (
	 		<ContextBuah.Provider value = {[fruit, setFruit, inputForm, setInputForm]}>
	 			{props.children}
	 		</ContextBuah.Provider>
	 	);
	};
