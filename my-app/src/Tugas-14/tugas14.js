import React from "react"
import {FruitProvider} from "./Tugas-14/ContextBuah"
import ContextList from "./Tugas-14/ContextList"
import ContextForm from "./Tugas-14/ContextForm"

const Fruit = () =>{
  return(
    <FruitProvider>
      <ContextList/>
      <ContextForm/>
    </FruitProvider>
  )
}

export default tugas14